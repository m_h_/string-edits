﻿Function CapitaliseFirstOption {

    [CmdletBinding()]
    param ($string)

    $tempString = $string.SubString(0, 1).ToUpper()
    $length = $string.length
    $tempString2 = $string.SubString(1, $length - 1)
    $string = $tempString + $tempString2

    $history +=  $string

    Clear-Host
    Write-Host $string -foreground Yellow

    Return $string

}