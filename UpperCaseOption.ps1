﻿Function UpperCaseOption {

    [CmdletBinding()]
    param ($string)
    
    $string = $string.ToString().ToUpper()
    
    Clear-Host
    Write-Host $string -foreground Yellow
    
    Return $string  
}