﻿Function DeleteOption  {

    [CmdletBinding()]
    param ($string)

    $cut = Read-host -Prompt '    Delete what? (Cut exact match)'
    $string = $string.Replace($cut, '')

    Clear-Host
    Write-Host $string -foreground Yellow   
    
    Return $string
}