﻿Function BReakOption {

    [CmdletBinding()]
    param ($string)

    Write-Host "    Insert Break where? " -foreground Green -NoNewLine
    $where = Read-Host 
    $flag = "%%%" + $where
    $string = $string.Replace($where, $flag)
    $file =  $env:USERPROFILE + '\tempBRfile.txt'
    $string = $string.Split('%%%', [StringSplitOptions]::RemoveEmptyEntries) | Out-File $file
    $string = Get-Content $file
    Remove-Item $file
    
    Clear-Host
    $string 

    Return $string
}