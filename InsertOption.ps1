﻿Function InsertOption {

    [CmdletBinding()]
    param ($string)
    
    Write-Host "    Insert what? "  -foreground Green -NoNewLine
    $insert = Read-host

    Write-Host "    Insert where? "  -foreground Green            
    Write-Host '[B] ' -foreground red -NoNewLine
    Write-Host 'Beginning of phrase' -foreground Gray            
    Write-Host '[E] ' -foreground red -NoNewLine
    Write-Host 'End of phrase' -foreground Gray            
    Write-Host '[S] ' -foreground red -NoNewLine
    Write-Host 'Start of every line' -foreground Gray            
    Write-Host '[C] ' -foreground red -NoNewLine
    Write-Host 'Close of every line' -foreground Gray
    Write-Host '[I] ' -foreground red -NoNewLine
    Write-Host 'Insert at a specific location' -foreground Gray            
    Write-Host '[W] ' -foreground red -NoNewLine
    Write-Host 'After each Word' -foreground Gray  

    $choice = (Read-Host).ToString().ToLower()

    If($choice -eq 'b') {
        $string = $insert + $string
    }
    ElseIf ($choice.ToString().ToLower() -eq 'e') {
        $string = $string + $insert
    }
    ElseIF($choice -eq 'w') {
        $temp = $string.Split(" ")  | ForEach { $_ + $insert }
        $temp = [String]::Join("",$temp)
        $string = $temp
    }
    ElseIF($choice -eq 's') { #not yet implemented
               
    }
    ElseIf($choice -eq 'c') {  #not yet implemented
        $string = $string.Replace("`n", $insert  + "`n")
    }
    ElseIf($choice -eq 'i') {
        Write-Host "    Insert where? "  -foreground Green
        $where = Read-Host
        $replacement = $insert + $where
        $string = $string.Replace($where, $replacement)
    }
    Else {
        Write-Host "    $($where) not found in '$($string)'" -foreground Red
    }

    Clear-Host
    Write-Host $string -foreground Yellow

    Return $string

}
