﻿Function ABCOption {

    [CmdletBinding()]
    param ($string)
    
    $file = $env:USERPROFILE + '\tempSortingOutput.txt'
    $string | Out-File $file

    $string = Get-Content $file | Sort-Object
    
    Clear-Host
    Write-Host $string -foreground Yellow  

    Remove-Item $file

    Return $string
}