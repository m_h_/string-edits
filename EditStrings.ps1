﻿#features to impleenmtn
#save a set of changes and apply to every line in a list
#insert character at each character
#option to revert to a specific history iteration

Clear-Host

# Import Modules
. "$PSScriptRoot\MainMenu.ps1" 
. "$PSScriptRoot\InsertOption.ps1" 
. "$PSScriptRoot\DeleteOption.ps1" 
. "$PSScriptRoot\ReplaceOption.ps1" 
. "$PSScriptRoot\SwapOption.ps1" 
. "$PSScriptRoot\ExtractOption.ps1" 
. "$PSScriptRoot\LowerCaseOption.ps1" 
. "$PSScriptRoot\UpperCaseOption.ps1" 
. "$PSScriptRoot\ProperCaseOption.ps1" 
. "$PSScriptRoot\CapitaliseFirstOption.ps1"
. "$PSScriptRoot\ABCOption.ps1" 
. "$PSScriptRoot\PrintHistoryOption.ps1" 
. "$PSScriptRoot\RevertHistoryOption.ps1"

# Create history for history tracking
$history = New-Object System.Collections.Generic.List[System.Object]
$string = $null

# Main program
While ($true) {
    # First message
    Write-Host "Enter a word or phrase: " -foreground Green -NoNewLine
    
    While($string -eq $null){
        $string = Read-Host
    }
    
    $history.Add($string)
    $continue = $true

    While($continue -eq $true) {
        # Main Menu
        ''
        $MainMenuChoice = MainMenu  

        If($MainMenuChoice -eq 'i') {
            # Insert Menu
            $string = InsertOption($string.Trim())
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'd') {
            # Delete Menu
            $string = DeleteOption($string)
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'r') {
            # ReplaceOption  
            $string = ReplaceOption($string)
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 's') {
            # Swap Menu
            $string = SwapOption($string)
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'e') {
            # ExtractOption
            $string = ExtractOption($string)     
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'lc') {
            # LowerCase Option
            $string = LowerCaseOption($string)
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'uc') {
            # UpperCaseOption
            $string = UpperCaseOption($string)
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'pc') {
            # Proper Case 
            $string = ProperCaseOption($string)
            $history.Add($string) 
        }
        ElseIf ($MainMenuChoice -eq 'cf') {
            # Capitalise first Option
            $string = CapitaliseFirstOption($string)
            $history.Add($string)
        }        
        ElseIf($MainMenuChoice -eq 'br') { #not implemented, check undo after completion.
            # BreakOption
            $string = BreakOption($string)
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'abc') {
            # ABCOption
            $string = ABCOption($string)
            $history.Add($string)
        } 
        ElseIf($MainMenuChoice -eq 'u') {
            # Undo Option
            If($history.Count -ne 0) {
                $string = $history[$history.length]
            }
            Clear-Host
            Write-Host $string -foreground Yellow
        } 
        ElseIf($MainMenuChoice -eq 'n') {
            # New phrase Option
            Write-Host "    Are you sure? This will remove ability to undo... [Y]/[N]" -foreground Red
            $confirmation = Read-Host
            if($confirmation.ToString().ToLower() -eq 'y') { 
                Clear-Host
                $history = new-object system.collections.history
                Break
            }
            Clear-Host
            Write-Host $string -foreground Yellow
        } 
        ElseIf($MainMenuChoice -eq 'c') {
            # Copy to Clipboard Option
            $string | clip
            Clear-Host
            Write-Host "'$($string)' copied to clipboard" -foreground Yellow
        } 
        ElseIf($MainMenuChoice -eq 'ph') {
           # Print history Menu
           PrintHistoryOption($string)
        } 
        ElseIf ($MainMenuChoice -eq 'rh') {
            # RevertHistoryOption          
            $string = RevertHistoryOption($string)
            $history.Add($string)
        } Else {
            Clear-Host
            Write-Host $string -foreground Yellow
        }
    }
}
