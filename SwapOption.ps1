﻿ Function SwapOption {

    [CmdletBinding()]
    param ($string)

    $swap1 = Read-Host -Prompt "    Swap what?"
    $swap2 = Read-Host -Prompt "    With what?"

    $tempString1 = $string.Replace($swap1, "X")
    $tempString2 = $tempString1.Replace($swap2, $swap1)
    $string = $tempString2.Replace("X", $swap2)
    
    Clear-Host
    Write-Host $string -foreground Yellow 

    Return $string
}