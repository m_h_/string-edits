﻿Function ReplaceOption {

    [CmdletBinding()]
    param ($string)
    
    $cut = Read-host -Prompt '    Replace what? (Delete exact match)'
    $replaceWith = Read-Host -Prompt '    Replace with what?'
    $string = $string.Replace($cut, $replaceWith)

    Clear-Host
    Write-Host $string -foreground Yellow   

    Return $String
}