﻿# Converts
# Matt Hanson
# to
# Hanson, Matt; 

#$list = Get-Content "C:\Users\mghp\list.txt" | Where {$_ -ne "" } | ForEach {$_.Trim()}

$list = Read-Host -Prompt 'Paste the list.'

$result = @()
ForEach($name in $list) {

    $updatedName = $name.Replace(" ", ";").Split(";", 2)
    $newName = $updatedName[1] + ", " + $updatedName[0] + ";"
    $result += $newName
}

$result

