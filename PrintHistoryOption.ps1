﻿Function PrintHistoryOption {

    [CmdletBinding()]
    param ($string)
    
    Clear-Host

    $lengthOfArray = $history.length

    For($i = 0; $i -lt $history.Length; $i++) {

        Write-Host "Iteration ", $i, ": " -NoNewline -Separator ""
        Write-Host $history[$lengthOfArray - 1] -ForegroundColor Green
        $lengthOfArray -= 1
    }

    Write-Host "Current:" $string -foreground Yellow

}