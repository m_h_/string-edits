﻿Function ProperCaseOption {
    
    [CmdletBinding()]
    param ($string)

    Write-Host $string.ToString().ToTitleCase -foreground Yellow
    $TextInfo = (Get-Culture).TextInfo
    $string = $TextInfo.ToTitleCase($string.ToLower())
    Clear-Host
    Write-Host $string -foreground Yellow 

    Return $string

}