﻿Function MainMenu {
    ''
    Write-Host '[I] ' -foreground red -NoNewLine
    Write-Host 'Insert ' -foreground Gray -NoNewLine         
    Write-Host '[D] ' -foreground red -NoNewLine
    Write-Host 'Delete ' -foreground Gray -NoNewLine   
    Write-Host '[R] ' -foreground red -NoNewLine
    Write-Host 'Replace ' -foreground Gray -NoNewLine   
    Write-Host '[S] ' -foreground red -NoNewLine
    Write-Host 'Swap' -foreground Gray

    Write-Host '[E] '-foreground red -NoNewLine
    Write-Host 'Extract Parts' -foreground Gray

    Write-Host '[LC] ' -foreground red -NoNewLine
    Write-Host 'Lower Case ' -foreground Gray -NoNewLine
    Write-Host '[UC] ' -foreground red -NoNewLine
    Write-Host 'Upper Case ' -foreground Gray

    Write-Host '[PC] ' -foreground red -NoNewLine
    Write-Host 'Proper Case ' -foreground Gray -NoNewLine
    Write-Host '[CF] ' -foreground red -NoNewLine
    Write-Host 'Capitalise First word only' -foreground Gray

    Write-Host '[BR] ' -foreground red -NoNewLine
    Write-Host 'Insert a New Line (break)' -foreground Gray    
        
    Write-Host '[ABC] ' -foreground red -NoNewLine
    Write-Host 'Sort Lines alphabetically' -foreground Gray     
              
    Write-Host '[U] ' -foreground red -NoNewLine        
    Write-Host 'Undo ' -foreground Gray -NoNewLine        
    Write-Host '[N] ' -foreground red -NoNewLine
    Write-Host 'New phrase (start over)' -foreground Gray         
        
    Write-Host '[C] ' -foreground red -NoNewLine   
    Write-Host 'Copy to Clipboard ' -foreground Gray     
        
    Write-Host '[PH] ' -foreground red -NoNewLine   
    Write-Host 'Print History to Screen ' -foreground Gray 
    Write-Host '[RH] ' -foreground re -NoNewLine
    Write-Host 'Revert to History iteration' -foreground Gray  

    Write-Host ''
    Write-Host "Choice: " -NoNewLine -foreground Green

    $option = (Read-Host).ToString().ToLower().Trim()
    
    Return $option
}