﻿Function ExtractOption {

    [CmdletBinding()]
    param ($string)

    $length = $string.length
    $continue = $true

    While($continue -eq $true) {
        $what = (Read-host -Prompt '    Extract from [B]eginning, [M]iddle, or [End]?').ToString().ToLower()

        If($what -eq 'b'){

            $howMuch = [int](Read-host -Prompt '    Extract how many characters?').ToString().ToLower()
            $string = $string.Substring(0, $howMuch)
            $continue = $false

        } ElseIf($what -eq 'm') {

            $where = Read-Host -Prompt "    Extract where? (Specify first matching search term)"
            $location = $string.IndexOf($where)

            If($location -eq -1) {
                    
                Write-Host "Not found. Try again."

            } Else {
                $howMuch = [int](Read-Host -Prompt "    Extract how many characters?")
                $string = $string.Substring($location, $howMuch)
                $continue = $false
            }

        } ElseIf($what -eq 'e') { 

            $howMuch = [int](Read-host -Prompt '    Extract how many characters?').ToString().ToLower()
            $string = $string.Substring($length - $howMuch, $howMuch)
            $continue = $false

        } ElseIf($what -eq 'q') {

            $continue = $false 
       
        } Else {

            Write-Host "Invalid choice. [Q]uit to main menu."
        }

    }
    
    Clear-Host
    Write-Host $string -foreground Yellow 

    Return $string
}