﻿Function RevertHistoryOption {

    [CmdletBinding()]
    param ($string)
            
    PrintHistoryOption($string)
    ''
    Write-Host "Which iteration do you want to revert to?"
    $iteration = Read-Host
    $string = $history[$iteration + 1]
    $history += $string
            
    Clear-Host
    Write-Host $string -foreground Yellow

    Return $string
}