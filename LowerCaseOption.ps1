﻿Function LowerCaseOption {
    
    [CmdletBinding()]
    param ($string)

    $string = $string.ToString().ToLower() 

    Clear-Host
    Write-Host $string -foreground Yellow  
    
    Return $string
}